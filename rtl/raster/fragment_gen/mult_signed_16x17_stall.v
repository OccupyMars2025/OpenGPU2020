module mult_16x17_stall
(
input			clk,
input			rst_n,
input 			busy,
input[15:0]		dina,
input[16:0]		dinb,
output[31:0]	dout
);


//`ifdef ASIC
DW_mult_pipe #(.a_width(16),.b_width(17),.num_stages(3),.stall_mode(1),.rst_mode(1))DW_mult_pipe_uut(.clk(clk),.rst_n(rst_n),.en(~busy),.tc(1'b0),.a(dina),.b(dinb),.product(dout));
/*`else
wire[39:0]	dout_temp;

reg[39:0]	dout_ff1;
reg[39:0]	dout_ff2;
reg[39:0]	dout_ff3;
reg[39:0]	dout_ff4;
reg[39:0]	dout_ff5;

assign		dout_temp = dina * dinb;

always@(posedge clk or negedge rst_n)
begin
	if(!rst_n)
		begin
			dout_ff1 <= `DLY 40'b0;
		    dout_ff2 <= `DLY 40'b0;
		    dout_ff3 <= `DLY 40'b0;
		    dout_ff4 <= `DLY 40'b0;
		    dout_ff5 <= `DLY 40'b0;	
		end
	else if(!busy)
		begin
			dout_ff1 <= `DLY dout_temp ;
		    dout_ff2 <= `DLY dout_ff1  ;
		    dout_ff3 <= `DLY dout_ff2  ;
		    dout_ff4 <= `DLY dout_ff3  ;
		    dout_ff5 <= `DLY dout_ff4  ;			
		end
end

assign	dout = dout_ff5;
`endif*/

endmodule
