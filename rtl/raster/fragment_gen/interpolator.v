//////////////////////////////////////////////////////////////////////////////////
// Company: USTC
// Engineer: yangyi
// 
// Create Date:    16:19:08 04/10/2008 
// Design Name: 
// Module Name:    interpolator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module interpolator_rgb(clk,rst_n,int_en,busy, a0, d_a1, d_a2, k1, k2, a_out, out_valid);
    input clk;
    input rst_n;
    input int_en;
    input busy;
    input [7:0] a0;
    input signed [8:0] d_a1;
    input signed [8:0] d_a2;
    input [31:0] k1;
    input [31:0] k2;
    output reg [7:0] a_out;
    output reg out_valid;
	
	wire signed [40:0] signed_a0 = {1'b0, a0, 32'b0};
	wire signed [32:0] signed_k1 = {1'b0, k1};
	wire signed [32:0] signed_k2 = {1'b0, k2};
	
	wire signed [40:0] result = signed_a0 + signed_k1 * d_a1 + signed_k2 * d_a2;
	
	always@(posedge clk or negedge rst_n)
	begin
		if(~rst_n)
			a_out <= 8'b0;
		else if(~busy && int_en)
            a_out <= result[39:32];
	end

    always@(posedge clk or negedge rst_n)
	begin
		if(~rst_n)
			out_valid <= 32'b0;
		else if(~busy)
            out_valid <= int_en;
	end 

endmodule
