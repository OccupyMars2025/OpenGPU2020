//-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   raster
//File Name     :   fragment_gen_flat.v
//Module name   :   fragment_gen_flat
//Full name     :   generate fragment for flat mode
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/6/1
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------  
module fragment_gen_flat(
    input clk,
    input rst_n,
    input busy,
    input [31:0] vertex2_z               ,
    input signed [31:0] vertex2_s              ,
    input signed [31:0] vertex2_t              ,
    input [7:0]  vertex2_primary_r       ,
    input [7:0]  vertex2_primary_g      ,
    input [7:0]  vertex2_primary_b      ,
    input [7:0]  vertex2_primary_a      ,
    input [31:0]					x_in,
    input [31:0]					y_in,
    input 						xy_in_en,
    input 							mode_end,
    output reg [31:0]					 x_out,
    output reg [31:0]					 y_out,
    output reg [31:0]                    z_out,
    output reg   				    xy_out_en,
    output reg [7:0]  primary_r                ,
    output reg [7:0]  primary_g                ,
    output reg [7:0]  primary_b                ,
    output reg [7:0]  primary_a                ,
    output reg [31:0] s                       ,
    output reg [31:0] t                       ,
    output reg fragment_gen_end          
); 

always@(posedge clk or negedge rst_n)
begin
    if(~rst_n)
    begin
        x_out              <= 32'b0;
        y_out              <= 32'b0;
        z_out              <= 32'b0;
        xy_out_en          <= 1'b0;
        primary_r          <= 8'b0;    
        primary_g          <= 8'b0;    
        primary_b          <= 8'b0;    
        primary_a          <= 8'b0;    
        s                  <= 32'b0;    
        t                  <= 32'b0;    
        fragment_gen_end   <= 1'b0;             
    end
    else if(~busy)
    begin
        x_out              <= x_in           ;
        y_out              <= y_in           ;
        z_out              <= z_in           ;
        xy_out_en          <= xy_in_en       ;
        primary_r          <= vertex2_primary_r       ;  
        primary_g          <= vertex2_primary_g       ;  
        primary_b          <= vertex2_primary_b       ;  
        primary_a          <= vertex2_primary_a       ;  
        s                  <= vertex2_s               ;   
        t                  <= vertex2_t               ;   
        fragment_gen_end   <= mode_end;
    end
end

endmodule
