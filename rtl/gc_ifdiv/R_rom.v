//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   ifdiv
//File Name     :   R_rom.v
//Module name   :   R_rom
//Full name     :   float number table 
//
//Author        :   xiang tian
//Email         :   
//Data          :   2020/3/7
//Version       :   V1.0
//
//Abstract      :   Read an Lut Table,calc rcp mantissa & exponent
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//
//-----------------------------------------------------------------------------

//-----------------------------
//DEFINE MACRO
//-----------------------------   
module R_rom(
       clka,
       addra,
       douta
);

input             clka;
input [4:0]       addra;
output reg [19:0] douta;



always @*
begin
    case(addra)
        5'd0 : douta <= 20'h000FE;
        5'd1 : douta <= 20'h0093A;
        5'd2 : douta <= 20'h015B7;
        5'd3 : douta <= 20'h027B4;
        5'd4 : douta <= 20'h03FF1;
        5'd5 : douta <= 20'h053AF;
        5'd6 : douta <= 20'h0776C;
        5'd7 : douta <= 20'h093AA;
        5'd8 : douta <= 20'h0B3A8;
        5'd9 : douta <= 20'h0D7A6;
        5'd10: douta <= 20'h0FFE4;
        5'd11: douta <= 20'h115E3;
        5'd12: douta <= 20'h145A1;
        5'd13: douta <= 20'h15F60;
        5'd14: douta <= 20'h1975E;
        5'd15: douta <= 20'h1B59D;
        5'd16: douta <= 20'h1D59C;
        5'd17: douta <= 20'h1F71B;
        5'd18: douta <= 20'h21A9A;
        5'd19: douta <= 20'h23FD9;
        5'd20: douta <= 20'h26758;
        5'd21: douta <= 20'h29117;
        5'd22: douta <= 20'h2BD16;
        5'd23: douta <= 20'h2EB55;
        5'd24: douta <= 20'h2EB15;
        5'd25: douta <= 20'h31C94;
        5'd26: douta <= 20'h35093;
        5'd27: douta <= 20'h35013;
        5'd28: douta <= 20'h38792;
        5'd29: douta <= 20'h3C1D1;
        5'd30: douta <= 20'h3C211;
        5'd31: douta <= 20'h3FFD0;
    endcase
end

endmodule    
//module R_rom
//#( parameter ROM_WIDTH = 20,
//   parameter ROM_ADDR_BITS = 5
//)
//(
//       clka,
//       addra,
//       douta
// );
//
//input                      clka;
//input [ROM_ADDR_BITS-1:0]  addra;
//output reg [ROM_WIDTH-1:0] douta;
//
//
//reg [ROM_WIDTH-1:0] lut_rom [(2**ROM_ADDR_BITS)-1:0];
//
//initial
//      $readmemh("R.txt", lut_rom, 0, 31);
//
//always @*
//begin
//    douta = lut_rom[addra];
//end
//
//endmodule
