
 //-----------------------------------------------------------------------------
//
//Copyright(c) 2020, ThorsianWay Technologies Co, Ltd
//All rights reserved.
//
//IP Name       :   pixel_shader
//File Name     :   shading_texture_gen.v
//Module name   :   shading_texture_gen
//Full name     :   pixel texture maping generator
//
//Author        :   zha daolu
//Email         :   
//Data          :   2020/5/13
//Version       :   V1.00
//
//Abstract      :   
//                  
//Called  by    :   GPU
//
//Modification history
//-----------------------------------------------------
//1.00: intial version 
//
//-----------------------------------------------------------------------------   
module shading_texture_gen(
    rst_n                     ,       //input reset, low active                                       
    clk                       ,       //input clock 
    busy_in                   ,
    uv_fifo_empty             ,       //input uv fifo empty                                           
    xy_fifo_empty             ,       //input xy fifo empty                                           
    u                         ,       //input u from uv fifo, 24.8                                    
    v                         ,       //input v from uv fifo, 24.8                                    
    uv_fifo_rd                ,       //output uv fifo read                                           
    tex_width                 ,       //texture width,support up to 2^24-1                            
    tex_height                ,       //teture height,support up to 2^24-1                            
    tex_addr                  ,       //current texture address in memory                             
    block_addr0               ,       //texel 0 cache line address , for bilinear filter              
    in_block_addr0            ,       //texel 0 in cache line address                                 
    block_addr1               ,       //texel 1 cache line address , for bilinear filter              
    in_block_addr1            ,       //texel 1 in cache line address                                 
    block_addr2               ,       //texel 2 cache line address , for bilinear filter              
    in_block_addr2            ,       //texel 2 in cache line address                                 
    block_addr3               ,       //texel 3 cache line address , for bilinear filter              
    in_block_addr3            ,       //texel 3 in cache line address                                 
    addr_fifo_w_en            ,       //texel address valid, 1: bilinear enable, 0: address valid     
    addr_fifo_almost_full     ,       //address fifo in cache full                                    
    x_in                      ,       //from xy fifo, input fragment x coordinate                     
    y_in                      ,       //from xy fifo, input fragment y coordinate                     
    z_in                      ,       //from xy fifo, input fragment z coordinate                     
    fog_x_in                  ,       //from xy fifo, input fragment x fog coordinate                 
    fog_y_in                  ,       //from xy fifo, input fragment y fog coordinate                 
    fog_z_in                  ,       //from xy fifo, input fragment z fog coordinate                 
    r_in                      ,       //from xy fifo, input fragment color compment r                 
    g_in                      ,       //from xy fifo, input fragment color compment g                 
    b_in                      ,       //from xy fifo, input fragment color compment b                 
    a_in                      ,       //from xy fifo, input fragment color compment a                 
    xy_fifo_rd                ,       //xy fifo read                                                  
    texel_32_0                ,       //texel 0 value                                                  
    texel_32_1                ,       //texel 1 value                                                 
    texel_32_2                ,       //texel 2 value                                                 
    texel_32_3                ,       //texel 3 value                                                 
    texel_32_en               ,       //texel value valid                                             
    busy_out                  ,       //output busy  
    r_out_gen                 ,       //output texel r value                                          
    g_out_gen                 ,       //output texel g value                                          
    b_out_gen                 ,       //output texel b value                                          
    a_out_gen                 ,       //output texel a value                                          
    x_out                     ,       //output fragment x coordinate                                  
    y_out                     ,       //output fragment y coordinate                                  
    z_out                     ,       //output fragment z coordinate                                  
    fog_x_out                 ,       //output fragment x fog coordinate                              
    fog_y_out                 ,       //output fragment y fog coordinate                              
    fog_z_out                 ,       //output fragment z fog coordinate                              
    r_out                     ,       //output fragment color compment r                              
    g_out                     ,       //output fragment color compment g                              
    b_out                     ,       //output fragment color compment b                              
    a_out                     ,       //output fragment color compment a                              
    pixel_out_en                      //output valid                                                  
);
input rst_n                            ;
input clk                              ;
input busy_in                          ;
input uv_fifo_empty                    ;
input xy_fifo_empty                    ;
input [31:0] u                         ;
input [31:0] v                         ;
output uv_fifo_rd                      ;
input [31:0] tex_width                 ;
input [31:0] tex_height                ;
input [31:0] tex_addr                  ;
output [31:0] block_addr0              ;
output [6:0] in_block_addr0            ;
output [31:0] block_addr1              ;
output [6:0] in_block_addr1            ;
output [31:0] block_addr2              ;
output [6:0] in_block_addr2            ;
output [31:0] block_addr3              ;
output [6:0] in_block_addr3            ;
output [1:0] addr_fifo_w_en            ;
input addr_fifo_almost_full            ;
input [31:0] x_in                      ;
input [31:0] y_in                      ;
input [31:0] z_in                      ;
input [31:0] fog_x_in                  ;
input [31:0] fog_y_in                  ;
input [31:0] fog_z_in                  ;
input [7:0]  r_in                      ;
input [7:0]  g_in                      ;
input [7:0]  b_in                      ;
input [7:0]  a_in                      ;
output xy_fifo_rd                      ;
input [31:0] texel_32_0                ;
input [31:0] texel_32_1                ;
input [31:0] texel_32_2                ;
input [31:0] texel_32_3                ;
input texel_32_en                      ;
output [7:0] r_out_gen                 ;
output [7:0] g_out_gen                 ;
output [7:0] b_out_gen                 ;
output [7:0] a_out_gen                 ;
output [31:0] x_out                    ;
output [31:0] y_out                    ;
output [31:0] z_out                    ;
output [31:0] fog_x_out                ;
output [31:0] fog_y_out                ;
output [31:0] fog_z_out                ;
output [7:0]  r_out                    ;
output [7:0]  g_out                    ;
output [7:0]  b_out                    ;
output [7:0]  a_out                    ;
output pixel_out_en                    ;
output busy_out                        ;


wire [7:0] param1;
wire [7:0] param2;
wire [7:0] param3;
wire [7:0] param4;
wire param_fifo_w_en;
wire param_en;
wire param_fifo_almost_full;
wire param_en_fifo2pixelout;
wire [7:0] param1_fifo2pixelout;
wire [7:0] param2_fifo2pixelout;
wire [7:0] param3_fifo2pixelout;
wire [7:0] param4_fifo2pixelout;
wire param_fifo_rd;

//transform u v to texel address
tex_addr u_addr_translate(
    .clk(clk),                                       //input clock
    .rst_n(rst_n),                                   //input reset, low active
    .uv_fifo_empty(uv_fifo_empty),                   //input uv fifo empty        
    .u(u),                                           //input u from uv fifo, 24.8        
    .v(v),                                           //input v from uv fifo, 24.8 
    .uv_fifo_rd(uv_fifo_rd),                         //output uv fifo read                                      
    .tex_width(tex_width),                           //texture width,support up to 2^24-1                       
    .tex_height(tex_height),                         //teture height,support up to 2^24-1                       
    .tex_addr(tex_addr),                             //current texture address in memory                        
    .block_addr0(block_addr0),                       //texel 0 cache line address , for bilinear filter         
    .in_block_addr0(in_block_addr0),                 //texel 0 in cache line address                            
    .block_addr1(block_addr1),                       //texel 1 cache line address , for bilinear filter         
    .in_block_addr1(in_block_addr1),                 //texel 1 in cache line address                            
    .block_addr2(block_addr2),                       //texel 2 cache line address , for bilinear filter         
    .in_block_addr2(in_block_addr2),                 //texel 2 in cache line address                            
    .block_addr3(block_addr3),                       //texel 3 cache line address , for bilinear filter         
    .in_block_addr3(in_block_addr3),                 //texel 3 in cache line address                            
    .addr_fifo_w_en(addr_fifo_w_en),                 //texel address valid, 1: bilinear enable, 0: address valid
    .addr_fifo_almost_full(addr_fifo_almost_full),   //address fifo in cache full                               
    .param1(param1),                                 //bilinear parameter1
    .param2(param2),                                 //bilinear parameter2
    .param3(param3),                                 //bilinear parameter3
    .param4(param4),                                 //bilinear parameter4
    .param_fifo_w_en(param_fifo_w_en),               //parameter fifo write enable
    .param_en(param_en),                             //parameter valid
    .param_fifo_almost_full(param_fifo_almost_full)  //parameter fifo almost full
);

//bilinear parameter fifo
param_fifo u_param_fifo(
    .SCAN_mode(1'b0)              ,
    .rst_n (rst_n)                     ,                   //input reset, low active
    .clk   (clk  )                     ,                   //input clock
    .param1(param1)                     ,                  //bilinear parameter1             
    .param2(param2)                     ,                  //bilinear parameter2             
    .param3(param3)                     ,                  //bilinear parameter3             
    .param4(param4)                     ,                  //bilinear parameter4             
    .param_fifo_w_en(param_fifo_w_en)            ,         //parameter fifo write enable     
    .param_en(param_en)                   ,                //parameter valid                 
    .param_fifo_almost_full(param_fifo_almost_full)     ,  //parameter fifo almost full      
    .addr_fifo_w_en(addr_fifo_w_en),                       //texel address valid, 1: bilinear enable, 0: address valid 
    .param_en_out(param_en_fifo2pixelout)               ,  //parameter fifo output parameter valid
    .param1_out(param1_fifo2pixelout)                 ,    //parameter fifo output parameter 1
    .param2_out(param2_fifo2pixelout)                 ,    //parameter fifo output parameter 2 
    .param3_out(param3_fifo2pixelout)                 ,    //parameter fifo output parameter 3 
    .param4_out(param4_fifo2pixelout)                 ,    //parameter fifo output parameter 4 
    .param_fifo_rd(param_fifo_rd)                          //parameter fifo read
);

//texel bilinear filter
pixel_out u_pixel_out(
    .rst_n(rst_n)                       ,      //input reset, low active   
    .clk(clk)                           ,      //input clock 
    .busy_in(busy_in)                   ,
    .tex_type(4'd5)                     ,      //input texture type, fixed to RGBA8888
    .xy_fifo_empty(xy_fifo_empty)       ,      //xy fifo empty
    .uv_end()                           ,      //process end
    .x_in(x_in)                         ,      //from xy fifo, input fragment x coordinate    
    .y_in(y_in)                         ,      //from xy fifo, input fragment y coordinate
    .z_in(z_in)                         ,      //from xy fifo, input fragment z coordinate                                         
    .fog_x_in(fog_x_in)                 ,      //from xy fifo, input fragment x fog coordinate    
    .fog_y_in(fog_y_in)                 ,      //from xy fifo, input fragment y fog coordinate    
    .fog_z_in(fog_z_in)                 ,      //from xy fifo, input fragment z fog coordinate    
    .r_in(r_in)                         ,      //from xy fifo, input fragment color compment r    
    .g_in(g_in)                         ,      //from xy fifo, input fragment color compment g    
    .b_in(b_in)                         ,      //from xy fifo, input fragment color compment b    
    .a_in(a_in)                         ,      //from xy fifo, input fragment color compment a  
    .xy_fifo_rd(xy_fifo_rd)             ,      //xy fifo read
    .param_en_in(param_en_fifo2pixelout),      //parameter fifo output parameter valid
    .param1_in(param1_fifo2pixelout)    ,      //parameter fifo output parameter 1    
    .param2_in(param2_fifo2pixelout)    ,      //parameter fifo output parameter 2    
    .param3_in(param3_fifo2pixelout)    ,      //parameter fifo output parameter 3    
    .param4_in(param4_fifo2pixelout)    ,      //parameter fifo output parameter 4    
    .param_fifo_rd(param_fifo_rd)       ,      //parameter fifo read                  
    .texel_32_0(texel_32_0)             ,      //texel 0 value          
    .texel_32_1(texel_32_1)             ,      //texel 1 value          
    .texel_32_2(texel_32_2)             ,      //texel 2 value          
    .texel_32_3(texel_32_3)             ,      //texel 3 value          
    .texel_32_en (texel_32_en)          ,      //texel value valid 
    .busy_out(busy_out)                 ,
    .r_out_gen(r_out_gen)               ,      //output texel r value   
    .g_out_gen(g_out_gen)               ,      //output texel g value   
    .b_out_gen(b_out_gen)               ,      //output texel b value   
    .a_out_gen(a_out_gen)               ,      //output texel a value   
    .x_out(x_out)                       ,      //output fragment x coordinate
    .y_out(y_out)                       ,      //output fragment y coordinate
    .z_out(z_out)                       ,      //output fragment z coordinate      
    .fog_x_out(fog_x_out)               ,      //output fragment x fog coordinate  
    .fog_y_out(fog_y_out)               ,      //output fragment y fog coordinate  
    .fog_z_out(fog_z_out)               ,      //output fragment z fog coordinate  
    .r_out(r_out)                       ,      //output fragment color compment r  
    .g_out(g_out)                       ,      //output fragment color compment g  
    .b_out(b_out)                       ,      //output fragment color compment b  
    .a_out(a_out)                       ,      //output fragment color compment a  
    .pixel_out_en(pixel_out_en )               //output fragment valid
);



endmodule
