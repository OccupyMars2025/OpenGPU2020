module distribute_fifo(
	datain, 
	rd, 
	wr, 
	rst_n, 
	clk, 
	dataout, 
	full, 
	empty,
    almost_empty,
    almost_full
);
parameter  DWIDTH = 32;
parameter  DSIZE  = 4;
parameter  AFULL  = 2**DSIZE-1;
parameter  AEMPTY = 1;

localparam  DSIZE_EXT = 2**DSIZE;
localparam  FULLSIZE = DSIZE_EXT ;//- 1'b1;

input [DWIDTH-1:0] datain;
input rd, wr, rst_n, clk;
output [DWIDTH-1:0] dataout;
output empty;
output reg full;
output reg almost_full;
output reg almost_empty;
wire [DWIDTH-1:0] dataout;
reg full_in, empty_in;
reg afull_in;
reg [DWIDTH-1:0] mem [DSIZE_EXT-1:0];
reg [DSIZE-1:0] rp, wp;
reg [DSIZE:0] datacnt;
integer i;

assign empty = empty_in;
// memory read out
assign dataout = mem[rp];
// memory write in
always@(posedge clk or negedge rst_n) begin
    if(!rst_n) begin
        for(i=0;i<DSIZE_EXT;i=i+1)
            mem[i] <= {DSIZE{1'b0}};
    end
    else if(wr && ~full_in) mem[wp]<=datain;
end
// memory write pointer increment
always@(posedge clk or negedge rst_n) begin
    if(!rst_n) wp<=0;
    else begin
      if(wr && ~full_in) wp<= wp+1'b1;
    end
end
// memory read pointer increment
always@(posedge clk or negedge rst_n)begin
    if(!rst_n) rp <= 0;
    else begin
      if(rd && ~empty_in) rp <= rp + 1'b1;
    end
end

// fifo data remain count
always @(posedge clk or negedge rst_n)begin
    if(!rst_n) datacnt <= 0;
    else case({rd,wr})
        2'b00: datacnt <= datacnt;
        2'b01: if(datacnt != FULLSIZE ) datacnt <= datacnt + 1'b1;
        2'b10: if(datacnt != {(DSIZE+1){1'b0}} ) datacnt <= datacnt - 1'b1;
        2'b11: datacnt <= datacnt;
    endcase
end



// Full signal generate
always @(*) full = full_in;

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) full_in <= 1'b1;
    else begin
      if( (~rd && wr)&&(datacnt == FULLSIZE -1'b1)) full_in <= 1'b1;
      else if((datacnt == FULLSIZE) && (rd && ~wr)) full_in <= 1'b0;
      else if(datacnt!= FULLSIZE) full_in <= 1'b0;
    end
end

always @(*) almost_full = afull_in;

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) afull_in <= 1'b1;
    else begin
      if((datacnt == AFULL-2'd1) && (~rd && wr)) afull_in <= 1'b1;
      else if ((datacnt == AFULL ) && (rd && ~wr)) afull_in <= 1'b0;
      else if (datacnt < AFULL ) afull_in <= 1'b0;
    end
end

// Empty signal generate
always@(posedge clk or negedge rst_n) begin
    if(!rst_n) empty_in <= 1'b1;
    else begin
      if((rd && ~wr) && (datacnt == 1)) empty_in <= 1'b1;
      else if(empty_in && wr) empty_in <= 1'b0;
    end
end

always@(posedge clk or negedge rst_n) begin
    if(!rst_n) almost_empty <= 1'b1;
    else begin
      if(( rd && ~wr ) && (datacnt == AEMPTY + 1'b1)) almost_empty <= 1'b1;
      else if(( ~rd && wr ) && (datacnt == AEMPTY )) almost_empty <= 1'b0;
    end
end

wire  emrd/*synthesis syn_keep=1*/;
wire  fuwr/*synthesis syn_keep=1*/;
assign  emrd = empty && rd;
assign  fuwr = full && wr;      

always @(posedge clk)
begin
    if(empty && rd) 
    begin
        $display("%m, fifo empty read.\n");
   // #1000;
   //     $stop;
    end
    if(full && wr)
    begin
        $display("%m, fifo full write.\n");
    //    #1000;
    //    $stop;
 
    end
end         

endmodule
