`timescale 1ns/1ps 
module fragment_gen_tb;


    reg clk;
    reg rst_n;
    wire busy;
    wire [95:0] ee0;
    wire [95:0] ee1;
    wire [95:0] ee2;
    wire signed[47:0] dz_02                        ;
    wire signed[47:0] dw_02                        ;
    wire signed[8:0]  dprimary_r_02                ;
    wire signed[8:0]  dprimary_g_02                ;
    wire signed[8:0]  dprimary_b_02                ;
    wire signed[8:0]  dprimary_a_02                ;
    wire signed[8:0]  dsecondary_a_02             ;
    wire signed[31:0] dfog_x_02                    ;
    wire signed[31:0] dfog_y_02                    ;
    wire signed[31:0] dfog_z_02                    ;
    wire signed[31:0] dfog_w_02                    ;
    wire signed[47:0] dz_12                        ;
    wire signed[47:0] dw_12                        ;
    wire signed[31:0] ds0_02                       ;
    wire signed[31:0] dt0_02                       ;
    wire signed[31:0] dr0_02                       ;
    wire signed[31:0] dq0_02                       ; 
    wire signed[31:0] ds0_12                       ;
    wire signed[31:0] dt0_12                       ;
    wire signed[31:0] dr0_12                       ;
    wire signed[31:0] dq0_12                       ;
    wire signed[8:0]  dprimary_r_12                ;
    wire signed[8:0]  dprimary_g_12                ;
    wire signed[8:0]  dprimary_b_12                ;
    wire signed[8:0]  dprimary_a_12                ;
    wire signed[31:0] dfog_x_12                    ;
    wire signed[31:0] dfog_y_12                    ;
    wire signed[31:0] dfog_z_12                    ;
    wire signed[31:0] dfog_w_12                    ;
    wire signed[95:0] area                         ;
    wire [31:0]					x_in;
    wire [31:0]					y_in;
    wire 						xy_in_en;
    wire 							mode_end;
    wire [31:0]					 x_out;
    wire [31:0]					 y_out;
    wire [31:0]                    z_out;
    wire 					    xy_out_en;
    wire [7:0]  primary_r                ;
    wire [7:0]  primary_g                ;
    wire [7:0]  primary_b                ;
    wire [7:0]  primary_a                ;
    wire [31:0] s0                       ;
    wire [31:0] t0                       ;
    wire [31:0] r0                       ;
    wire [31:0] fog_x                    ;
    wire [31:0] fog_y                    ;
    wire [31:0] fog_z                    ;
    wire [31:0] fog_w                    ;
    wire  fragment_gen_end               ;
wire bte_start;
wire [31:0] tile_x;                      
wire [31:0] tile_y;                      
wire [47:0] vertex0_x               ;    
wire [47:0] vertex0_y               ;    
wire [47:0] vertex0_z               ;    
wire [47:0] vertex0_w               ;    
wire [31:0] vertex0_s0              ;    
wire [31:0] vertex0_t0              ;    
wire [31:0] vertex0_r0              ;    
wire [31:0] vertex0_q0              ;    
wire [7:0]  vertex0_primary_r       ;    
wire [7:0]  vertex0_primary_g      ;     
wire [7:0]  vertex0_primary_b      ;     
wire [7:0]  vertex0_primary_a      ;     
wire [31:0] vertex0_fog_x          ;     
wire [31:0] vertex0_fog_y          ;     
wire [31:0] vertex0_fog_z          ;     
wire [31:0] vertex0_fog_w          ;     
wire [47:0] vertex1_x               ;    
wire [47:0] vertex1_y               ;    
wire [47:0] vertex1_z               ;    
wire [47:0] vertex1_w               ;    
wire [31:0] vertex1_s0              ;    
wire [31:0] vertex1_t0              ;    
wire [31:0] vertex1_r0              ;    
wire [31:0] vertex1_q0              ;    
wire [7:0]  vertex1_primary_r       ;    
wire [7:0]  vertex1_primary_g      ;     
wire [7:0]  vertex1_primary_b      ;     
wire [7:0]  vertex1_primary_a      ;     
wire [31:0] vertex1_fog_x          ;     
wire [31:0] vertex1_fog_y          ;     
wire [31:0] vertex1_fog_z          ;     
wire [31:0] vertex1_fog_w          ;     
wire [47:0] vertex2_x               ;    
wire [47:0] vertex2_y               ;    
wire [47:0] vertex2_z               ;    
wire [47:0] vertex2_w               ;    
wire [31:0] vertex2_s0              ;    
wire [31:0] vertex2_t0              ;    
wire [31:0] vertex2_r0              ;    
wire [31:0] vertex2_q0              ;    
wire [7:0]  vertex2_primary_r       ;    
wire [7:0]  vertex2_primary_g      ;     
wire [7:0]  vertex2_primary_b      ;     
wire [7:0]  vertex2_primary_a      ;     
wire [31:0] vertex2_fog_x          ;     
wire [31:0] vertex2_fog_y          ;     
wire [31:0] vertex2_fog_z          ;     
wire [31:0] vertex2_fog_w          ; 

wire [31:0] s2;
wire [31:0] t2;
wire [31:0] w2;

BTE bte_uut
(
    .clk                    (clk                 ) ,                              
    .rst_n                  (rst_n               ) ,                            
    .busy                   (busy                ) ,                             
    .bte_start              (bte_start           ) ,                        
    .tile_x                 (tile_x              ) ,                           
    .tile_y                 (tile_y              ) ,                           
    .vertex0_x              (vertex0_x           ) ,         
    .vertex0_y              (vertex0_y           ) ,         
    .vertex0_z              (vertex0_z           ) ,         
    .vertex0_w              (vertex0_w           ) ,         
    .vertex0_s0             (vertex0_s0          ) ,         
    .vertex0_t0             (vertex0_t0          ) ,         
    .vertex0_primary_r      (vertex0_primary_r   ) ,
    .vertex0_primary_g      (vertex0_primary_g   ) ,
    .vertex0_primary_b      (vertex0_primary_b   ) ,
    .vertex0_primary_a      (vertex0_primary_a   ) ,
    .vertex1_x              (vertex1_x           ) ,
    .vertex1_y              (vertex1_y           ) ,
    .vertex1_z              (vertex1_z           ) ,
    .vertex1_w              (vertex1_w           ) ,
    .vertex1_s0             (vertex1_s0          ) ,
    .vertex1_t0             (vertex1_t0          ) ,
    .vertex1_primary_r      (vertex1_primary_r   ) ,
    .vertex1_primary_g      (vertex1_primary_g   ) ,
    .vertex1_primary_b      (vertex1_primary_b   ) ,
    .vertex1_primary_a      (vertex1_primary_a   ) ,
    .vertex2_x              (vertex2_x           ) ,
    .vertex2_y              (vertex2_y           ) ,
    .vertex2_z              (vertex2_z           ) ,
    .vertex2_w              (vertex2_w           ) ,
    .vertex2_s0             (vertex2_s0          ) ,
    .vertex2_t0             (vertex2_t0          ) ,
    .vertex2_primary_r      (vertex2_primary_r   ) ,
    .vertex2_primary_g      (vertex2_primary_g   ) ,
    .vertex2_primary_b      (vertex2_primary_b   ) ,
    .vertex2_primary_a      (vertex2_primary_a   ) ,
    .dz_02                  (dz_02               ) ,
    .dw_02                  (dw_02               ) ,
    .ds_02                 (ds0_02              ) ,
    .dt_02                 (dt0_02              ) ,
    .dprimary_r_02          (dprimary_r_02       ) ,
    .dprimary_g_02          (dprimary_g_02       ) ,
    .dprimary_b_02          (dprimary_b_02       ) ,
    .dprimary_a_02          (dprimary_a_02       ) ,
    .dz_12                  (dz_12               ) ,
    .dw_12                  (dw_12               ) ,
    .ds_12                 (ds0_12              ) ,
    .dt_12                 (dt0_12              ) ,
    .dprimary_r_12          (dprimary_r_12       ) ,
    .dprimary_g_12          (dprimary_g_12       ) ,
    .dprimary_b_12          (dprimary_b_12       ) ,
    .dprimary_a_12          (dprimary_a_12       ) ,
    .x_out                  (x_in               ) ,
    .y_out                  (y_in               ) ,
    .xy_out_en              (xy_in_en           ) ,
    .ee0                    (ee0                 ) ,
    .ee1                    (ee1                 ) ,
    .ee2                    (ee2                 ) ,
    .mode_end               (mode_end            ) ,
    .bte_busy               (bte_busy            ) ,
    .area                   (area                ) ,
    .z_slope                (z_slope             ) ,
    .s2(s2),
    .t2(t2),
    .w2(w2)
);

fragment_gen_smooth uut(
    .clk                          (clk                   ),
    .rst_n                        (rst_n                 ),
    .busy                         (busy                  ),
    .vertex2_z                    (vertex2_z             ),
    .vertex2_w                    (w2             ),
    .vertex2_s                   (s2            ),
    .vertex2_t                   (t2            ),
    .vertex2_primary_r            (vertex2_primary_r     ),
    .vertex2_primary_g            (vertex2_primary_g     ),
    .vertex2_primary_b            (vertex2_primary_b     ),
    .vertex2_primary_a            (vertex2_primary_a     ),
    .ee0                          (ee0                   ),
    .ee1                          (ee1                   ),
    .ee2                          (ee2                   ),
    .dz_02                        (dz_02                 ),
    .dw_02                        (dw_02                 ),
    .dprimary_r_02                (dprimary_r_02         ),
    .dprimary_g_02                (dprimary_g_02         ),
    .dprimary_b_02                (dprimary_b_02         ),
    .dprimary_a_02                (dprimary_a_02         ),
    .dsecondary_a_02              (dsecondary_a_02       ),
    .dz_12                        (dz_12                 ),
    .dw_12                        (dw_12                 ),
    .ds_12                       (ds0_12                ),
    .dt_12                       (dt0_12                ),
    .ds_02                       (ds0_02                ),
    .dt_02                       (dt0_02                ), 
    .dprimary_r_12                (dprimary_r_12         ),
    .dprimary_g_12                (dprimary_g_12         ),
    .dprimary_b_12                (dprimary_b_12         ),
    .dprimary_a_12                (dprimary_a_12         ),
    .area                         (area                  ),
    .x_in                         (x_in                  ),
    .y_in                         (y_in                  ),
    .xy_in_en                     (xy_in_en              ),
    .mode_end                     (mode_end              ),
    .x_out                        (x_out                 ),
    .y_out                        (y_out                 ),
    .z_out                        (z_out                 ),
    .xy_out_en                    (xy_out_en             ),
    .primary_r                    (primary_r             ),
    .primary_g                    (primary_g             ),
    .primary_b                    (primary_b             ),
    .primary_a                    (primary_a             ),
    .s                           (s0                    ),
    .t                           (t0                    ),
    .depth_offset_enable          (0),
    .factor(0),
    .unit(0),
    .fragment_gen_end             (fragment_gen_end      )
);  
wire test_end;
fragment_gen_checker fragment_gen_checker_uut(
    .clk(clk),
    .rst_n_in(rst_n),
    .test_end(test_end),
    .busy(busy),                             //input busy
    .bte_start              (bte_start          ) ,                        //input traversal start
    .tile_x                 (tile_x             ) ,                           //input cur tile left-bottom x coordinate, fixed 16.16
    .tile_y                 (tile_y             ) ,                           //input cur tile left-bottom y coordinate, fixed 16.16 
    .vertex0_x              (vertex0_x          ) ,         //input vertex0 x coordinate, fixed 1.31.16
    .vertex0_y              (vertex0_y          ) ,         //input vertex0 x coordinate, fixed 1.31.16
    .vertex0_z              (vertex0_z          ) ,         //input vertex0 x coordinate, fixed 1.31.16
    .vertex0_w              (vertex0_w          ) ,         //input vertex0 x coordinate, fixed 1.31.16
    .vertex0_s0             (vertex0_s0         ) ,         //input vertex0 x coordinate, fixed 1.15.16
    .vertex0_t0             (vertex0_t0         ) ,         //input vertex0 x coordinate, fixed 1.15.16
    .vertex0_r0             (vertex0_r0         ) ,         //input vertex0 x coordinate, fixed 1.15.16
    .vertex0_q0             (vertex0_q0         ) ,         //input vertex0 x coordinate, fixed 1.15.16
    .vertex0_primary_r      (vertex0_primary_r  ) ,
    .vertex0_primary_g      (vertex0_primary_g  ) ,
    .vertex0_primary_b      (vertex0_primary_b  ) ,
    .vertex0_primary_a      (vertex0_primary_a  ) ,
    .vertex0_fog_x          (vertex0_fog_x      ) ,
    .vertex0_fog_y          (vertex0_fog_y      ) ,
    .vertex0_fog_z          (vertex0_fog_z      ) ,
    .vertex0_fog_w          (vertex0_fog_w      ) ,
    .vertex1_x              (vertex1_x          ) ,
    .vertex1_y              (vertex1_y          ) ,
    .vertex1_z              (vertex1_z          ) ,
    .vertex1_w              (vertex1_w          ) ,
    .vertex1_s0             (vertex1_s0         ) ,
    .vertex1_t0             (vertex1_t0         ) ,
    .vertex1_r0             (vertex1_r0         ) ,
    .vertex1_q0             (vertex1_q0         ) ,
    .vertex1_primary_r      (vertex1_primary_r  ) ,
    .vertex1_primary_g      (vertex1_primary_g  ) ,
    .vertex1_primary_b      (vertex1_primary_b  ) ,
    .vertex1_primary_a      (vertex1_primary_a  ) ,
    .vertex1_fog_x          (vertex1_fog_x      ) ,
    .vertex1_fog_y          (vertex1_fog_y      ) ,
    .vertex1_fog_z          (vertex1_fog_z      ) ,
    .vertex1_fog_w          (vertex1_fog_w      ) ,
    .vertex2_x              (vertex2_x          ) ,
    .vertex2_y              (vertex2_y          ) ,
    .vertex2_z              (vertex2_z          ) ,
    .vertex2_w              (vertex2_w          ) ,
    .vertex2_s0             (vertex2_s0         ) ,
    .vertex2_t0             (vertex2_t0         ) ,
    .vertex2_r0             (vertex2_r0         ) ,
    .vertex2_q0             (vertex2_q0         ) ,
    .vertex2_primary_r      (vertex2_primary_r  ) ,
    .vertex2_primary_g      (vertex2_primary_g  ) ,
    .vertex2_primary_b      (vertex2_primary_b  ) ,
    .vertex2_primary_a      (vertex2_primary_a  ) ,
    .vertex2_fog_x          (vertex2_fog_x      ) ,
    .vertex2_fog_y          (vertex2_fog_y      ) ,
    .vertex2_fog_z          (vertex2_fog_z      ) ,
    .vertex2_fog_w          (vertex2_fog_w      ) ,
    .x_out                  (x_out              ) ,
    .y_out                  (y_out              ) ,
    .z_out                  (z_out              ) ,
    .xy_out_en              (xy_out_en          ) ,
    .primary_r              (primary_r          ) ,
    .primary_g              (primary_g          ) ,
    .primary_b              (primary_b          ) ,
    .primary_a              (primary_a          ) ,
    .in_s0                  (s0              ) ,
    .in_t0                  (t0              ) ,
    .fragment_gen_end       (fragment_gen_end   ) ,
    .bte_busy               (bte_busy           )
);
always #5 clk = ~clk;

initial
begin
   clk = 0;
   rst_n = 0;
   #100 @(posedge clk) rst_n = 1;
   wait(test_end && ~bte_busy)
   #1000 $stop;
end

initial
begin
    
	$fsdbDumpfile("fragment_wave.fsdb");
	$fsdbDumpvars(0,fragment_gen_tb);
end

endmodule
